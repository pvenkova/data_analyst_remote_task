The current repository contains the following  files: 

1. **e-commerce.R** - Script for transofrming the input data the new suppliers into the target data.
2. **data_analysis_task_onedot.pdf** - Slides describing the process for transforming the data
3. **Target_Data.xlsx** - target data with the pre-defined data structure used to transform the input data
4. **supplier_car.json** - input data, provided by the new suppliers 
5. **supplier_car.xlsx** - output data transformed by e-commerce.R having the same data structure as the target data (Target_Data.xlsx)


The repository is Rstudio project; for users of Rstudio; git clone the full repository with all of the  files and Rstudio will do the magic for you; no need to transform paths to data and e.t.c.
 

